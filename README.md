# Text Processor

Next generation text processor intending to replace `LaTeX` in the long run.

This project is still in its very early stages.

Although a very powerful tool, `LaTeX` frequently fails to deliver easy and satisfying results in fringe cases owing to its root `TeX` being non-trivial.

This project is intended to provide a lightweight and fast modular unicode text-processor with a powerful, yet intuitive, set of core instructions. Easy-to-use customisation will be achieved by using the capabilities of some common high-level programming language. It should produce beautiful documents according to typographic rules. Like `LaTeX` it should be multi-purpose with applications in academia, business and every-day usage.

This project will be implemented using `Rust`.
