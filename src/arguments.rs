/* Text Processor
 * Copyright (C) 2021  Lukas Matthias Bartl, Fabian Lukas Grubmüller
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//! functionality for dealing with argument parsing

use clap::{crate_version, App, Arg};

/// `struct` for storing command line arguments
pub struct Arguments {
    pub show_license: bool,
    pub input_file: std::path::PathBuf,
}

/// return a `struct` containing the given command line arguments
pub fn arguments() -> Arguments {
    let matches = App::new("Text-Processor")
        .version(crate_version!())
        .author("Lukas Matthias Bartl, Fabian Lukas Grubmüller")
        .about("Typeset beautiful documents")
        .arg(
            Arg::with_name("input")
                .help("Specify the input file")
                .index(1)
                .value_name("INPUT"),
        )
        .arg(
            Arg::with_name("license")
                .help("Display all the licenses used in this project.")
                .short("l")
                .long("license")
                .takes_value(false),
        )
        .get_matches();

    let input_file = matches.value_of("input").unwrap_or("");
    let input_file = std::path::PathBuf::from(input_file);

    let show_license = matches.is_present("license");

    Arguments {
        show_license,
        input_file,
    }
}
